#utility
def parse_input_list(filename)
  File.readlines(filename).map(&:strip)
end

#part one
def at_least_three_vowels?(string)
  !(/^.*[aeiou].*[aeiou].*[aeiou].*$/ =~ string).nil?
end

def at_least_one_letter_twice_in_a_row?(string)
  !(/([A-Za-z])\1/ =~ string).nil?
end

def does_not_contain_forbidden_patterns?(string)
  !(/^((?!ab|cd|pq|xy).)*$/ =~ string).nil?
end

def nice_word?(string)
  at_least_three_vowels?(string) &&
  at_least_one_letter_twice_in_a_row?(string) &&
  does_not_contain_forbidden_patterns?(string)
end

def nice_string_count
  strings = parse_input_list("inputs/day05.txt")
  strings.count { |string| nice_word?(string) }
end

#part two
def pair_of_two_letters_appears_twice?(string)
  !(/^.*(.{2}).*\1.*$/ =~ string).nil?
end

def letter_repeats_with_one_letter_between?(string)
  !(/^.*([a-zA-Z]).\1.*$/ =~ string).nil?
end

def new_condition_nice_word?(string)
  pair_of_two_letters_appears_twice?(string) &&
  letter_repeats_with_one_letter_between?(string)
end

def new_condition_nice_string_count
  strings = parse_input_list("inputs/day05.txt")
  strings.count { |string| new_condition_nice_word?(string) }
end
