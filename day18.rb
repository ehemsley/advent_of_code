def input
  File.readlines("inputs/day18.txt").map(&:strip)
end

class GameOfLife
  attr_accessor :rows_amount, :columns_amount, :grid

  def initialize(rows_amount, columns_amount)
    @rows_amount = rows_amount
    @columns_amount = columns_amount
    @grid = initialize_grid
  end

  def initialize_grid
    Array.new(rows_amount) { Array.new(columns_amount, false) }
  end

  def parse_configuration(input)
    input.each_with_index do |line, index|
      @grid[index] = line.split('').map { |light| light == '#' ? true : false }
    end
  end

  def cell(i, j)
    @grid[i][j]
  end

  #need to push false if out of bounds...
  def neighbors(i, j)
    [].tap do |neighbors|
      neighbors << (i < @rows_amount - 1 ? cell(i+1, j) : false)
      neighbors << (j < @columns_amount - 1 ? cell(i, j+1) : false)
      neighbors << (i > 0 ? cell(i-1, j) : false)
      neighbors << (j > 0 ? cell(i, j-1) : false)

      neighbors << ((i < @rows_amount - 1 && j < @columns_amount - 1) ? cell(i+1, j+1) : false)
      neighbors << ((i < @rows_amount - 1 && j > 0) ? cell(i+1, j-1) : false)

      neighbors << ((i > 0 && j < @columns_amount - 1) ? cell(i-1, j+1) : false)
      neighbors << ((i > 0 && j > 0) ? cell(i-1,j-1) : false)
    end
  end

  def step(steps = 1)
    steps.times do
      step_once
    end
    return
  end

  def step_with_broken_corners(steps = 1)
    steps.times do
      step_once
      set_corners
    end
    return
  end

  def on_count
    @grid.map { |line| line.count(true) }.inject(:+)
  end

  def print
    puts @grid.map { |line| line.map { |cell| cell ? '#' : '.' }.join('') + "\n" }.join('')
  end

  private

  def step_once
    new_grid = initialize_grid

    @grid.each_with_index do |line, i|
      line.each_with_index do |cell, j|
        if cell
          on_neighbors = neighbors(i, j).count(true)
          new_grid[i][j] = (on_neighbors == 2 || on_neighbors == 3)
        else
          on_neighbors = neighbors(i, j).count(true)
          new_grid[i][j] = (on_neighbors == 3)
        end
      end
    end
    @grid = new_grid
  end

  def set_corners
    @grid[0][0] = true
    @grid[0][@columns_amount-1] = true
    @grid[@rows_amount-1][0] = true
    @grid[@rows_amount-1][@columns_amount-1] = true
  end
end

def part_one_solution
  gol = GameOfLife.new(100, 100)
  gol.parse_configuration(input)

  gol.step(100)
  gol.on_count
end

def part_two_solution
  gol = GameOfLife.new(100,100)
  gol.parse_configuration(input)

  gol.step_with_broken_corners(100)
  gol.on_count
end
