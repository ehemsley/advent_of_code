require 'graphunk'

def input
  File.readlines("inputs/day13.txt").map(&:strip)
end

def generate_graph(input)
  graph = Graphunk::WeightedUndirectedGraph.new
  input.each do |line|
    name_one, point_value, name_two = line.scan(/\b[A-Z][a-z]+|\d+/)
    point_value = point_value.to_i * (line.include?("gain") ? 1 : -1)
    graph.add_vertex(name_one) unless graph.vertex_exists?(name_one)
    graph.add_vertex(name_two) unless graph.vertex_exists?(name_two)
    if graph.edge_exists?(name_one, name_two)
      graph.adjust_weight(name_one, name_two, graph.edge_weight(name_one, name_two) + point_value)
    else
      graph.add_edge(name_one, name_two, point_value)
    end
  end
  graph
end

def find_maximum_happiness(graph)
  graph.vertices.permutation.map { |perm| perm << perm.first }.map do |arrangement|
    arrangement.each_cons(2).map { |pair| graph.edge_weight(pair[0], pair[1]) }.inject(:+)
  end.max
end

def part_one_solution
  graph = generate_graph(input)
  find_maximum_happiness(graph)
end

def part_two_solution
  graph = generate_graph(input)
  graph.add_vertex("Me")
  graph.vertices.reject { |v| v == "Me" }.each do |vertex|
    graph.add_edge(vertex, "Me", 0)
  end
  find_maximum_happiness(graph)
end
