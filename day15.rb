def input
  File.readlines("inputs/day15.txt").map(&:strip)
end

class Ingredient
  attr_reader :name, :capacity, :durability, :flavor, :texture, :calories

  def initialize(name, capacity, durability, flavor, texture, calories)
    @name = name
    @capacity = capacity.to_i
    @durability = durability.to_i
    @flavor = flavor.to_i
    @texture = texture.to_i
    @calories = calories.to_i
  end
end

def parse_input
  ingredients = []
  input.each do |line|
    name, capacity, durability, flavor, texture, calories = line.scan(/\b[A-Z][a-z]+|-?\d+/)
    ingredients << Ingredient.new(name, capacity, durability, flavor, texture, calories)
  end
  ingredients
end

def test
  butterscotch = Ingredient.new("Butterscotch", -1, -2, 6, 3, 8)
  cinnamon = Ingredient.new("Cinnamon", 2, 3, -2, -1, 3)
  solver = IngredientOptimizer.new([butterscotch, cinnamon], n_ints_that_sum_to_100(2))
  solver
end

def combinations_of_sum(sum, int_amount)
  combinations = []
  range = (0..sum).to_a
  range.product(*Array.new(int_amount-1, range)) do |nums|
    combinations << nums if nums.inject(:+) == sum
  end
  combinations
end

def deliciousness(ingredients, amounts)
  capacity = [ingredients.map(&:capacity).zip(amounts).map { |x| x.inject(:*) }.inject(:+), 0].max
  durability = [ingredients.map(&:durability).zip(amounts).map { |x| x.inject(:*) }.inject(:+), 0].max
  flavor = [ingredients.map(&:flavor).zip(amounts).map { |x| x.inject(:*) }.inject(:+), 0].max
  texture = [ingredients.map(&:texture).zip(amounts).map { |x| x.inject(:*) }.inject(:+), 0].max
  capacity * durability * flavor * texture
end

def calories(ingredients, amounts)
  [ingredients.map(&:calories).zip(amounts).map { |x| x.inject(:*) }.inject(:+), 0].max
end

def part_one_solution
  ingredients = parse_input
  combinations_of_sum(100, 4).map { |amounts| deliciousness(ingredients, amounts) }.max
end

def part_two_solution
  ingredients = parse_input
  combinations_of_sum(100, 4).map { |amounts| [deliciousness(ingredients, amounts), calories(ingredients, amounts)] }.select { |pair| pair[1] == 500 }.max_by { |pair| pair[0] }[0]
end
