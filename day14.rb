def input
  File.readlines("inputs/day14.txt").map(&:strip)
end

class ReindeerRace
  def initialize
    @deer = []
    @deer_distances = {}
    @deer_scores = {}
  end

  def add_deer(deer)
    @deer << deer
    @deer_distances[deer.name] = 0
    @deer_scores[deer.name] = 0
  end

  def simulate(duration)
    reset_race
    (1..duration).each do |step|
      update_deer_distances
    end
    @deer_distances
  end

  def simulate_with_new_rules(duration)
    reset_race
    (1..duration).each do |step|
      update_deer_distances
      @deer_scores[current_leader] += 1
    end
    @deer_scores
  end

  def reset_race
    @deer.each { |d| d.reset }
    @deer_distances.each { |k,v| v = 0 }
    @deer_scores.each { |k,v| v = 0 }
  end

  private

  def update_deer_distances
    @deer.each do |deer|
      if deer.resting?
        deer.rest
      else
        @deer_distances[deer.name] += deer.speed
        deer.fly
      end
    end
  end

  def current_leader
    @deer_distances.max_by { |k,v| v }[0]
  end
end

class Reindeer
  def initialize(name, speed, flying_duration, resting_duration)
    @name = name
    @speed = speed
    @flying_duration = flying_duration
    @resting_duration = resting_duration

    @remaining_flying_duration = flying_duration
    @remaining_resting_duration = resting_duration
    @resting = false
  end

  def name
    @name
  end

  def speed
    @speed
  end

  def resting?
    @resting
  end

  def rest
    @remaining_resting_duration -= 1
    if @remaining_resting_duration == 0
      @resting = false
      @remaining_flying_duration = @flying_duration
    end
    return
  end

  def fly
    @remaining_flying_duration -= 1
    if @remaining_flying_duration == 0
      @resting = true
      @remaining_resting_duration = @resting_duration
    end
    return
  end

  def reset
    @remaining_flying_duration = @flying_duration
    @remaining_resting_duration = @resting_duration
  end
end

def initialize_race
  race = ReindeerRace.new
  input.each do |line|
    name, speed, flying_duration, resting_duration = line.scan(/\b[A-Z][a-z]+|\d+/)
    new_deer = Reindeer.new(name, speed.to_i, flying_duration.to_i, resting_duration.to_i)
    race.add_deer(new_deer)
  end
  race
end

def part_one_solution
  initialize_race.simulate(2503).values.max
end

def part_two_solution
  initialize_race.simulate_with_new_rules(2503).values.max
end
