def weapons
  {
    'dagger' => { 'cost' => 8, 'damage' => 4, 'armor' => 0 },
    'shortsword' => { 'cost' => 10, 'damage' => 5, 'armor' => 0 },
    'warhammer' => { 'cost' => 25, 'damage' => 6, 'armor' => 0 },
    'longsword' => { 'cost' => 40, 'damage' => 7, 'armor' => 0 },
    'greataxe' => { 'cost' => 74, 'damage' => 8, 'armor' => 0 }
  }
end

def armor
  {
    'leather' => { 'cost' => 13, 'damage' => 0, 'armor' => 1 },
    'chainmail' => { 'cost' => 31, 'damage' => 0, 'armor' => 2 },
    'splintmail' => { 'cost' => 53, 'damage' => 0, 'armor' => 3 },
    'bandedmail' => { 'cost' => 75, 'damage' => 0, 'armor' => 4 },
    'platemail' => { 'cost' => 102, 'damage' => 0, 'armor' => 5 }
  }
end

def rings
  {
    'damage+1' => { 'cost' => 25, 'damage' => 1, 'armor' => 0 },
    'damage+2' => { 'cost' => 50, 'damage' => 2, 'armor' => 0 },
    'damage+3' => { 'cost' => 100, 'damage' => 3, 'armor' => 0 },
    'defense+1' => { 'cost' => 20, 'damage' => 0, 'armor' => 1 },
    'defense+2' => { 'cost' => 40, 'damage' => 0, 'armor' => 2 },
    'defense+3' => { 'cost' => 80, 'damage' => 0, 'armor' => 3 }
  }
end

def boss_stats
  {
    'hit_points' => 103,
    'damage' => 9,
    'armor' => 2
  }
end

def init_equipment
  weapons_and_armor = weapons.merge(armor)
  weapons_and_armor.merge(rings)
end

def weapon_and_armor_combinations
  weapons.keys.product(armor.keys) + weapons.keys.map { |x| [x] }
end

def ring_combinations
  rings.keys.combination(2).to_a + rings.keys.map { |x| [x] }
end

def combinations
  weapon_and_armor_combinations.product(ring_combinations).map { |list| list.flatten }
end

def stats(equipment_names)
  equipment = init_equipment
  stats = { 'hit_points' => 100, 'cost' => 0, 'damage' => 0, 'armor' => 0 }
  equipment_names.each do |name|
    stats = stats.merge(equipment[name]) { |k, oldval, newval| oldval + newval }
  end
  stats
end

def player_wins?(player_stats)
  boss_hp = boss_stats['hit_points']
  player_hp = player_stats['hit_points']
  while boss_hp > 0 and player_hp > 0
    boss_hp -= [player_stats['damage'] - boss_stats['armor'], 1].max
    return true if boss_hp <= 0
    player_hp -= [boss_stats['damage'] - player_stats['armor'], 1].max
    return false if player_hp <= 0
  end
end

def part_one_solution
  winning_equipment_combos = {}
  combinations.each do |combination|
    stats_from_equipment = stats(combination)
    winning_equipment_combos[combination] = stats_from_equipment['cost'] if player_wins?(stats_from_equipment)
  end
  winning_equipment_combos.min_by { |k, v| v }.last
end

def part_two_solution
  losing_equipment_combos = {}
  combinations.each do |combination|
    stats_from_equipment = stats(combination)
    losing_equipment_combos[combination] = stats_from_equipment['cost'] if !player_wins?(stats_from_equipment)
  end
  losing_equipment_combos.max_by { |k, v| v }.last
end
