def init_password_enumerator(seed)
  Enumerator.new do |y|
    pass = seed
    loop do
      y << pass
      pass = pass.succ
      pass = pass.succ while !valid_password?(pass)
    end
  end
end

def valid_password?(input)
  contains_three_straight?(input) && !contains_forbidden_characters?(input) && contains_two_unique_pairs?(input)
end

def contains_three_straight?(input)
  input.split('').each_cons(3).select { |triple| triple[0].succ == triple[1] && triple[1].succ == triple[2] }.any?
end

def contains_forbidden_characters?(input)
  (input =~ /^[^iol]+$/).nil?
end

def contains_two_unique_pairs?(input)
  input.split('').each_cons(2).select { |pair| pair[0] == pair[1] }.uniq.length > 1
end

def part_one_solution
  password_enumerator = init_password_enumerator("hepxcrrq")
  password_enumerator.take(2).last
end

def part_two_solution
  password_enumerator = init_password_enumerator("hepxcrrq")
  password_enumerator.take(3).last
end
