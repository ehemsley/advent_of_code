#utility
def parse_dimension(line_input)
  line_input.split("x").map(&:to_i)
end

def parse_dimension_list(filename)
  File.readlines(filename).map { |line| parse_dimension(line) }
end

#part one
def required_square_feet_for_box(dimensions)
  smallest_area = dimensions.min(2).inject(:*)
  surface_area = 2*dimensions[0]*dimensions[1] +
                 2*dimensions[1]*dimensions[2] +
                 2*dimensions[0]*dimensions[2]
  surface_area + smallest_area
end

def required_square_feet_from_list
  dimensions = parse_dimension_list("inputs/day02.txt")
  dimensions.map { |dimension| required_square_feet_for_box(dimension) }.inject(:+)
end

#part two
def required_length_for_ribbon(dimensions)
  wrapping_length = dimensions.min(2).inject(:+) * 2
  bow_length = dimensions.inject(:*)
  wrapping_length + bow_length
end

def required_length_from_list
  dimensions_list = parse_dimension_list("inputs/day02.txt")
  dimensions_list.map { | dimensions| required_length_for_ribbon(dimensions) }.inject(:+)
end
