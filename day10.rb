class LookAndSay
  def initialize(seed)
    @enumerator = initialize_enumerator(seed)
  end

  def enumerator
    @enumerator
  end

  def initialize_enumerator(seed)
    Enumerator.new do |y|
      next_num = seed
      loop do
        y << next_num
        next_num = look_and_say(next_num)
      end
    end
  end

  def look_and_say(string)
    [].tap do |groups|
      current_counter = 1
      chars = string.split('')
      chars.each_cons(2) do |pair|
        if pair[0] == pair[1]
          current_counter += 1
        else
          groups << [current_counter.to_s, pair[0]]
          current_counter = 1
        end
      end
      groups << [current_counter.to_s, chars.last]
    end.flatten.join('')
  end
end

def part_one_solution
  look_and_say = LookAndSay.new("1113122113")
  look_and_say.enumerator.take(41).last.length
end

def part_two_solution
  look_and_say = LookAndSay.new("1113122113")
  look_and_say.enumerator.take(51).last.length
end
