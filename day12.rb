require 'json'

def input
  File.open("inputs/day12.txt").read.strip
end

def find_and_add_all_numbers(string)
  string.scan(/-?\d+/).map(&:to_i).inject(:+)
end

def parse(input)
  JSON.parse(input)
end

def filter_reds(input)
  if input.is_a?(Array)
    input.each { |elt| filter_reds(elt) }
  elsif input.is_a?(Hash)
    input.each { |k,v| v == "red" ? input.clear : filter_reds(v) }
  end
end

def part_one_solution
  find_and_add_all_numbers(input)
end

def part_two_solution
  find_and_add_all_numbers(filter_reds(parse(input)).to_s)
end
