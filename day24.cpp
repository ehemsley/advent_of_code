#include <iostream>
#include <fstream>
#include <string>
#include <bitset>
#include <vector>
#include <math.h>
#include <numeric>

template<typename T1>
std::ostream& operator <<( std::ostream& out, const std::vector<T1>& object )
{
    out << "[";
    if ( !object.empty() )
    {
        for(typename std::vector<T1>::const_iterator
            iter = object.begin();
            iter != --object.end();
            ++iter) {
                out << *iter << ", ";
        }
        out << *--object.end();
    }
    out << "]";
    return out;
}

int vector_sum(std::vector<int>& list)
{
  return std::accumulate(list.begin(), list.end(), 0);
}

std::vector< std::vector<bool> > generate_partition_table(std::vector<int>& set)
{
  int sum = vector_sum(set);
  int size = set.size();

  std::vector< std::vector<bool> > results(sum+1, std::vector<bool>(sum+1, false));
  results[0][0] = true;

  for (int i = 0; i < size; i++) {
    for (int j = sum; j >= 0; j--) {
      for (int k = sum; k >= 0; k--) {
        if (results[j][k]) {
          if (j + set[i] <= sum) results[j + set[i]][k] = true;
          if (k + set[i] <= sum) results[j][k + set[i]] = true;
        }
      }
    }
  }

  return results;
}

bool equal_two_partition_exists(std::vector<int>& subset, std::vector< std::vector<bool> >& partition_table)
{
  int sum = vector_sum(subset);

  return partition_table[sum / 2][sum / 2];
}

bool equal_three_partition_exists(std::vector<int>& subset, std::vector< std::vector<bool> >& partition_table)
{
  int sum = vector_sum(subset);

  return partition_table[sum / 3][sum / 3];
}

std::vector<int> part_one(std::vector<int>& set)
{
  std::vector<int> result;
  std::vector<int> remaining_subset;
  std::vector< std::vector<bool> > partition_table = generate_partition_table(set);

  int subset_size = 0;
  int best_compartment_size = set.size();
  unsigned long best_quantum_entanglement = std::numeric_limits<size_t>::max();

  int weight_sum = vector_sum(set) / 3;

  int n = 0;
  std::bitset<28> subset_bits(n);

  while (n < (1 << set.size())) {

    do
    {
      n += 1;
      subset_bits = n;
      subset_size = subset_bits.count();
    }
    while (subset_size > best_compartment_size);

    int subset_sum = 0;

    for (int i = 0; i < subset_bits.size(); i++ ) {
      if (subset_bits[i]) subset_sum += set[i];
    }

    if (weight_sum == subset_sum)
    {
      remaining_subset.clear();

      for (int i = 0; i < subset_bits.size(); i++ ) {
        if (!subset_bits[i]) remaining_subset.push_back(set[i]);
      }

      if (equal_two_partition_exists(remaining_subset, partition_table))
      {
        if (subset_size <= best_compartment_size)
        {
          unsigned long quantum_entanglement = 1;
          for (int i = 0; i < subset_bits.size(); i++ ) {
            if (subset_bits[i]) quantum_entanglement *= set[i];
          }

          if (quantum_entanglement < best_quantum_entanglement || subset_size < best_compartment_size)
          {
            best_quantum_entanglement = quantum_entanglement;
            best_compartment_size = subset_size;

            result.clear();
            for (int i = 0; i < subset_bits.size(); i++ ) {
              if (subset_bits[i]) result.push_back(set[i]);
            }
          }
        }
      }
    }
  }

  return result;
}

std::vector<int> part_two(std::vector<int>& set)
{
  std::vector<int> result;
  std::vector<int> remaining_subset;
  std::vector< std::vector<bool> > partition_table = generate_partition_table(set);

  int subset_size = 0;
  int best_compartment_size = set.size();
  unsigned long best_quantum_entanglement = std::numeric_limits<size_t>::max();

  int weight_sum = vector_sum(set) / 4;

  int n = 0;
  std::bitset<28> subset_bits(n);

  while (n < (1 << set.size())) {
    do {
      n += 1;
      subset_bits = n;
      subset_size = subset_bits.count();
    }
    while (subset_size > best_compartment_size);

    int subset_sum = 0;

    for (int i = 0; i < subset_bits.size(); i++ ) {
      if (subset_bits[i]) subset_sum += set[i];
    }

    if (weight_sum == subset_sum)
    {
      remaining_subset.clear();

      for (int i = 0; i < subset_bits.size(); i++) {
        if (!subset_bits[i]) remaining_subset.push_back(set[i]);
      }

      if (equal_three_partition_exists(remaining_subset, partition_table))
      {
        if (subset_size <= best_compartment_size)
        {
          unsigned long quantum_entanglement = 1;
          for (int i = 0; i < subset_bits.size(); i++ ) {
            if (subset_bits[i]) quantum_entanglement *= set[i];
          }

          if (quantum_entanglement < best_quantum_entanglement || subset_size < best_compartment_size)
          {
            best_quantum_entanglement = quantum_entanglement;
            best_compartment_size = subset_size;

            result.clear();
            for (int i = 0; i < subset_bits.size(); i++ ) {
              if (subset_bits[i]) result.push_back(set[i]);
            }
          }
        }
      }
    }
  }

  return result;
}

int main(int argc, char *argv[])
{
  std::string line;
  std::ifstream file("inputs/day24.txt");
  std::vector<int> ints;

  while (std::getline(file, line))
      ints.push_back(stoi(line));
  file.close();

  std::vector<int> result;

  std::cout << "part one" << std::endl;
  result = part_one(ints);

  std::cout << result << std::endl;

  unsigned long product = 1;
  for (int i = 0; i < result.size(); i++) {
    product *= result[i];
  }

  std::cout << product << std::endl;

  std::cout << "part two" << std::endl;
  result.clear();
  result = part_two(ints);

  std::cout << result << std::endl;

  product = 1;
  for (int i = 0; i < result.size(); i++) {
    product *= result[i];
  }

  std::cout << product << std::endl;

  return 0;
}
