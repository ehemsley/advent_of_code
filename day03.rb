#utility

def input
  File.open("inputs/day03.txt").read.strip
end

#part one
class HouseGrid
  def initialize(input)
    x_dimension = 1
    y_dimension = 1
    x_start_modifier = 0
    y_start_modifier = 0
    chars = input.split("")
    chars.each do |char|
      if char == '<'
        x_dimension += 1
        x_start_modifier -= 1
      elsif char == '>'
        x_dimension += 1
        x_start_modifier += 1
      elsif char == '^'
        y_dimension += 1
        y_start_modifier -= 1
      elsif char == 'v'
        y_dimension += 1
        y_start_modifier += 1
      end
    end

    @grid = Array.new(x_dimension) { Array.new(y_dimension, 0) }
    @x_dimension = x_dimension
    @y_dimension = y_dimension
    @x_start_modifier = x_start_modifier
    @y_start_modifier = y_start_modifier
    @chars = chars
  end

  def houses_with_at_least_one_present
    x_position = @x_dimension / 2 + @x_start_modifier
    y_position = @y_dimension / 2 + @y_start_modifier
    @chars.each do |char|
      if char == '<'
        x_position -= 1
      elsif char == '>'
        x_position += 1
      elsif char == '^'
        y_position -= 1
      elsif char == 'v'
        y_position += 1
      end
      @grid[x_position][y_position] += 1
    end

    @grid.map { |list| list.select { |x| x > 0 }.length }.inject(:+)
  end
end

def part_one_solution
  house_grid = HouseGrid.new(input)
  house_grid.houses_with_at_least_one_present
end

#part two solution
class RoboSantaHouseGrid
  def initialize(input)
    x_dimension = 1
    y_dimension = 1
    x_start_modifier = 0
    y_start_modifier = 0
    chars = input.split("")
    santa_chars, robo_santa_chars = chars.partition.with_index { |el, i| i.even? }
    santa_chars.each do |char|
      if char == '<'
        x_dimension += 1
        x_start_modifier -= 1
      elsif char == '>'
        x_dimension += 1
        x_start_modifier += 1
      elsif char == '^'
        y_dimension += 1
        y_start_modifier -= 1
      elsif char == 'v'
        y_dimension += 1
        y_start_modifier += 1
      end
    end

    robo_santa_chars.each do |char|
      if char == '<'
        x_dimension += 1
      elsif char == '>'
        x_dimension += 1
      elsif char == '^'
        y_dimension += 1
      elsif char == 'v'
        y_dimension += 1
      end
    end

    @grid = Array.new(x_dimension) { Array.new(y_dimension, 0) }
    @x_dimension = x_dimension
    @y_dimension = y_dimension
    @x_start_modifier = x_start_modifier
    @y_start_modifier = y_start_modifier
    @santa_chars = santa_chars
    @robo_santa_chars = robo_santa_chars
  end

  def houses_with_at_least_one_present
    santa_x_position = @x_dimension / 2 + @x_start_modifier
    santa_y_position = @y_dimension / 2 + @y_start_modifier
    robo_santa_x_position = santa_x_position
    robo_santa_y_position = santa_y_position
    @santa_chars.each do |char|
      if char == '<'
        santa_x_position -= 1
      elsif char == '>'
        santa_x_position += 1
      elsif char == '^'
        santa_y_position -= 1
      elsif char == 'v'
        santa_y_position += 1
      end
      @grid[santa_x_position][santa_y_position] += 1
    end

    @robo_santa_chars.each do |char|
      if char == '<'
        robo_santa_x_position -= 1
      elsif char == '>'
        robo_santa_x_position += 1
      elsif char == '^'
        robo_santa_y_position -= 1
      elsif char == 'v'
        robo_santa_y_position += 1
      end
      @grid[robo_santa_x_position][robo_santa_y_position] += 1
    end

    @grid.map { |list| list.select { |x| x > 0 }.length }.inject(:+)
  end
end

def part_two_solution
  house_grid = RoboSantaHouseGrid.new(input)
  house_grid.houses_with_at_least_one_present
end
