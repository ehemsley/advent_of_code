require 'digest/md5'

def MD5_enumerator(input)
  Enumerator.new do |y|
    added_int = 1
    loop do
      y << Digest::MD5.hexdigest(input + added_int.to_s)
      added_int += 1
    end
  end
end

def find_first_hash_with_five_leading_zeroes(input)
  MD5_enumerator(input).take_while do |result_string|
    result_string.index("00000") == nil || result_string.index("00000") != 0
  end.length + 1
end

def find_first_hash_with_six_leading_zeroes(input)
  MD5_enumerator(input).take_while do |result_string|
    result_string.index("000000") == nil || result_string.index("000000") != 0
  end.length + 1
end
